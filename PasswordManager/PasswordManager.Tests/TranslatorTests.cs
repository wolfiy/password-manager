﻿/// ETML
/// Author: Sebastien TILLE
/// Date: April 23rd, 2024

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PasswordManager.Tests
{
    /// <summary>
    /// Tests of the <see cref="Translator"/> class.
    /// </summary>
    [TestClass]
    public class TranslatorTests
    {
        #region Caesar
        [TestMethod]
        public void CaesarEncryptsWithKey1()
        {
            Translator t = new Translator(1);

            var s = "aaa";
            var expected = "bbb";
            var actual = t.Caesar(s, true);
            Assert.AreEqual(expected, actual);

            var s2 = "ÿ";
            var expected2 = " ";
            var actual2 = t.Caesar(s2, true);
            Assert.AreEqual(expected2, actual2);

            var s3 = "@";
            var expected3 = "A";
            var actual3 = t.Caesar(s3, true);
            Assert.AreEqual(expected3, actual3);

            var s4 = " ";
            var expected4 = "!";
            var actual4 = t.Caesar(s4, true);
            Assert.AreEqual(expected4, actual4);

            var s5 = "a01234[];' !þÿ";
            var expected5 = "b12345\\^<(!\"ÿ ";
            var actual5 = t.Caesar(s5, true);
            Assert.AreEqual(expected5, actual5);
        }

        [TestMethod]
        public void CaesarDecryptsWithKey1()
        {
            Translator t = new Translator(1);
            var s = "bbb";
            var expected = "aaa";
            var actual = t.Caesar(s, false);
            Assert.AreEqual(expected, actual);

            var s2 = " ";
            var expected2 = "ÿ";
            var actual2 = t.Caesar(s2, false);
            Assert.AreEqual(expected2, actual2);

            var s3 = "A";
            var expected3 = "@";
            var actual3 = t.Caesar(s3, false);
            Assert.AreEqual(expected3, actual3);

            var s4 = "!";
            var expected4 = " ";
            var actual4 = t.Caesar(s4, false);
            Assert.AreEqual(expected4, actual4);

            var s5 = "b12345\\^<(!\"ÿ ";
            var expected5 = "a01234[];' !þÿ";
            var actual5 = t.Caesar(s5, false);
            Assert.AreEqual(expected5, actual5);
        }
        #endregion

        #region Vigenere
        [TestMethod]
        public void VigenereEncryptWithabc()
        {
            Translator t = new Translator(" !");
            var s = "aaaaaa";
            var expected = "ababab";
            var actual = t.Vigenere(s, true);
            //actual = t.EncryptVigenere(s);
            Assert.AreEqual(expected, actual);
        }
        #endregion
    }
}
