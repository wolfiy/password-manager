# SillySafe
A simple CLI based password manager using the Vigenere cipher. Part of the 
P_Sec class at ETML.

## Usage
A binary can be grabbed from the 
[releases page](https://gitlab.com/wolfiy/password-manager/-/releases). 
SillySafe can also be built from source using the .NET Framework 4.7.2.

Upon first launch, the program will prompt the user for a new master password. 
This will be used as the Vigenere key and stored in the `config.txt` file. 

Entries created through SillySafe will be stored in the `pass.txt` file.

### Functionality
As of version 1.1.0, SillySafe can add add, modify, delete and list entries.
On launch, the master password is checked and access is granted only upon 
entering the correct master password.

When changing the master password, copies of the config file and passwords "database" are created. Be careful as those will be overwritten if the password is changed again.

### Limitations
SillySafe cannot automatically generate passwords.
