﻿/// ETML
/// Author: Sebastien TILLE
/// Date: March 19th, 2024

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PasswordManager
{
    /// <summary>
    /// Handles file operations.
    /// </summary>
    internal class FileManager
    {
        #region Variables
        /// <summary>
        /// Separator used to differenciate entries.
        /// </summary>
        private const string ENTRY_SEPARATOR = "ENTRY";

        /// <summary>
        /// Path to the config file.
        /// </summary>
        private readonly string config;

        /// <summary>
        /// Password "database" path.
        /// </summary>
        private readonly string passwords;

        /// <summary>
        /// Prefixes used in the GUI ("Login", "Password", "URL").
        /// </summary>
        private readonly List<string> prefixes;

        /// <summary>
        /// The password manager instance from to work with.
        /// </summary>
        private readonly PasswordManager passwordManager;

        /// <summary>
        /// Translator used to convert plain text to cipher and cipher to 
        /// plain text.
        /// </summary>
        private Translator translator;
        #endregion

        /// <summary>
        /// Initializes a new file manager.
        /// </summary>
        /// <param name="config">Path to the config file.</param>
        /// <param name="passwords">Path to the password database.</param>
        public FileManager(PasswordManager passwordManager,
                           string passwords, string config, 
                           Translator translator)
        {
            this.passwordManager = passwordManager;
            this.config = config;
            this.passwords = passwords;
            this.translator = translator;
            prefixes = new List<string>
            {
                "",
                "Login: ",
                "Password: ",
                "URL: "
            };

            // Create database if file does not exist
            using (StreamWriter writer = File.AppendText(passwords)) 
                writer.Close();
        } // FileManager()

        #region Master password
        /// <summary>
        /// Lets the user change their master password.
        /// </summary>
        public void ChangeMasterPassword()
        {
            Helper.PrintHeader(Menu.MASTER);
            Console.WriteLine(Resources.Common.MasterAreYouSure);

            // Quit if no
            char key = Helper.GetUserKey();
            if (!(key == 'y' || key == 'Y'))
                return;

            // Backup old files
            Helper.BackupFile(passwords);
            Helper.BackupFile(config);

            // Choose new password
            string master;
            Console.Clear();
            do
            {
                Console.Clear();
                Console.Write(Resources.Common.MasterChoosePrompt);
                master = Console.ReadLine();
                Console.Write(Resources.Common.MasterChooseConfirm);
            }
            while (!master.Equals(Console.ReadLine()));

            // Update key in database
            StreamReader reader = new StreamReader(passwords);
            List<string> everything = new List<string>();

            while (!reader.EndOfStream)
                everything.Add(reader.ReadLine());
            reader.Close();

            var newTranslator = new Translator(master);

            for (int i = 0; i < everything.Count; ++i)
            {
                if (everything[i].StartsWith(ENTRY_SEPARATOR))
                {
                    // Decrypt
                    var plainHandle = translator.Vigenere(everything[i + 2], false);
                    var plainPass = translator.Vigenere(everything[i + 3], false);

                    // Encrypt using new key
                    var newHandle = newTranslator.Vigenere(plainHandle, true);
                    var newPass = newTranslator.Vigenere(plainPass, true);

                    // Add to file
                    everything[i + 2] = newHandle;
                    everything[i + 3] = newPass;
                }
            }

            // Update password file
            using (StreamWriter writer = new StreamWriter(passwords))
            {
                everything.ForEach(line =>  writer.WriteLine(line));
                writer.Close();
            }

            // Update config file
            using (StreamWriter writer = new StreamWriter(config))
            {
                writer.Write(newTranslator.Vigenere(master, true));
                writer.Close();
            }

            translator = newTranslator;

            Console.WriteLine("Master password successfully changed.");
            Console.WriteLine("Press any key to go back.");
            Console.ReadLine();
        } // ChangeMasterPassword()
        #endregion

        #region File management
        /// <summary>
        /// Gets all entries.
        /// </summary>
        /// <returns>A list containing all websites.</returns>
        public List<string> GetEntries()
        {
            List<string> entries = new List<string>();
            StreamReader reader = new StreamReader(passwords);

            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();

                if (line.StartsWith(ENTRY_SEPARATOR))
                    entries.Add(line.Remove(0, ENTRY_SEPARATOR.Length + 1));
            }

            reader.Close();

            return entries;
        } // GetEntries()

        /// <summary>
        /// Adds an entry to the database.
        /// </summary>
        /// <returns>The details of the new entry, unprefixed.</returns>
        public string AddEntry()
        {
            string name, login, password, url;

            Helper.PrintHeader(Menu.ADD);

            Console.Write(Resources.Common.MenuAddWebsite);
            name = Console.ReadLine();

            Console.Write(Resources.Common.MenuAddLogin);
            login = translator.Vigenere(Console.ReadLine(), true);

            Console.Write(Resources.Common.MenuAddPassword);
            password = translator.Vigenere(Console.ReadLine(), true);

            Console.Write(Resources.Common.MenuAddUrl);
            url = Console.ReadLine();

            // Inputs are merged to form the new entry's details
            Console.WriteLine();
            var entry = String.Join("\n", $"{ENTRY_SEPARATOR} {name}", name, login, password, url, "\n");

            // Trying to write new entry to the file
            try
            {
                File.AppendAllText(passwords, entry);
            }
            catch (Exception e)
            {
                Console.WriteLine(Resources.Common.MenuAddError, name);
                Console.WriteLine(e.ToString());
                return String.Empty;
            }

            // Wait for user input
            Console.WriteLine(Resources.Common.MenuAddConfirmation, name);
            Console.ReadKey(true);

            return entry;
        } // AddEntry()

        /// <summary>
        /// Removes an entry from the database. The entry has to be chosen by 
        /// the user.
        /// </summary>
        /// <returns>En empty string if the deletion failed, the entry's 
        /// details otherwise.</returns>
        public string RemoveEntry()
        {
            // Print title
            Helper.PrintHeader(Menu.DELETE);

            // Print all entries and select one
            ListAllEntries();
            string entry = SelectEntry();
            var toRemove = GetEntryDetails(entry);

            // Get existing file
            var file = File.ReadAllLines(passwords).ToList();

            var index = file.IndexOf("ENTRY " + entry);
            index = file.FindIndex(s => s == entry);
            file.RemoveRange(index - 1, 6);

            // Ask for confirmation
            Console.WriteLine(Resources.Common.MenuRemovePrompt, entry);
            char key = Helper.GetUserKey();

            if (key == 'y' || key == 'Y')
                File.WriteAllLines(passwords, file);
            else
                return String.Empty;

            return entry;
        } // RemoveEntry()

        /// <summary>
        /// Edits an entry.
        /// </summary>
        /// <returns>The modified entry.</returns>
        public string EditEntry()
        {
            // Print title
            Helper.PrintHeader(Menu.EDIT);

            // Print all entries and select which to edit
            ListAllEntries();
            string entry = SelectEntry();

            // Existing file
            var file = File.ReadAllLines(passwords).ToList();
            var index = file.IndexOf("ENTRY " + entry);

            // Select action
            Helper.PrintHeader(Menu.EDIT);
            Console.WriteLine(
                String.Format(Resources.Common.MenuEditSelected,
                entry)
            );
            Console.WriteLine();
            Console.WriteLine(Resources.Common.MenuEditSelection);
            Console.WriteLine();
            
            char key = Helper.GetUserKey();
            int subindex;
            bool isEncrypted = false;
            switch (key)
            {
                case '1':
                case '4':
                    subindex = int.Parse(key.ToString());
                    break;
                case '2':
                case '3':
                    isEncrypted = true;
                    subindex = int.Parse(key.ToString());
                    break;
                default:
                    EditEntry();
                    return String.Empty;
            }

            // Edit 
            Helper.PrintHeader(Menu.EDIT);
            Console.Write(Resources.Common.MenuEditPrompt);
            var edited = isEncrypted ? translator.Vigenere(Console.ReadLine(), true)
                                     : Console.ReadLine();
            
            file[index + subindex] = edited;
            File.WriteAllLines(passwords, file);

            return edited;
        } // EditEntry()
        #endregion

        #region Entry Display
        /// <summary>
        /// Displays a list of all entires.
        /// </summary>
        public void ListAllEntries()
        {
            var entries = GetEntries();
            Console.WriteLine(Resources.Common.ApplicationHelpSelection);
            for (int i = 0; i < entries.Count; ++i)
                Console.WriteLine($"{i + 1}. {entries[i]}");
            Console.WriteLine(Resources.Common.ApplicationHelpQuit);
        } // ListAllEntries()

        /// <summary>
        /// Displays the details of an entry. The entry has to be selected
        /// by the user.
        /// </summary>
        public void ListEntryDetails()
        {
            // Header
            Helper.PrintHeader(Menu.LIST);

            // Getting all entries from the password file then listing these
            ListAllEntries();

            // Select one entry and display its details
            string entry = SelectEntry();

            Console.Clear();
            GetEntryDetails(entry).ForEach(l => Console.WriteLine(l));

            // Exiting listing
            Console.WriteLine();
            Console.WriteLine(Resources.Common.PressAnyKey);
            Console.ReadKey(true);
        } // ListEntryDetails()
        #endregion

        #region Entry selection
        /// <summary>
        /// Gets the details of an entry.
        /// </summary>
        /// <param name="entry">The entry from which to get the details.</param>
        /// <returns>A list containing the entry's details, prefixed for 
        /// convenient GUI output.</returns>
        public List<string> GetEntryDetails(string entry)
        {
            // Getting lines from file
            var details = File.ReadLines(passwords)
                              .SkipWhile(line => !line.Equals($"{ENTRY_SEPARATOR} {entry}"))
                              .ToList()
                              .GetRange(1, 4);

            // Decipher encrypted text
            details[2] = translator.Vigenere(details[2], false);
            details[1] = translator.Vigenere(details[1], false);

            // Add prefixes to lines
            return details.Zip(prefixes, (d, r) => r + d).ToList();
        } // GetEntryDetails()

        /// <summary>
        /// Converts the index of an entry to its name.
        /// </summary>
        /// <returns>The name of the corresponding entry.</returns>
        public string SelectEntry()
        {
            // Selected index (value is one ahead of the list)
            int index;

            while (true)
            {
                var input = Console.ReadLine();

                if (int.TryParse(input, out index))
                {
                    Console.WriteLine(index);
                    break;
                }
                else if (input == "q")
                {
                    passwordManager.MainMenu();
                    break;
                }
               Console.WriteLine("Enter an integer to choose an entry, or <q> to abort.");
            }

            return GetEntries()[index - 1]; // TODO edge case
        } // SelectEntry
        #endregion
    }
}
