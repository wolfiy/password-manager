﻿/// ETML
/// Author: Sebastien TILLE
/// Date: April 16th, 2024

using System;
using System.IO;
using System.Text;

namespace PasswordManager
{
    /// <summary>
    /// Types of menu.
    /// </summary>
    public enum Menu
    {
        MAIN,   // Main menu
        LIST,   // Entries listing
        ADD,    // Entry addition
        DELETE, // Entry deletion
        EDIT,   // Entry edition
        MASTER  // Master password edition
    }

    /// <summary>
    /// Utility class used for various operations.
    /// </summary>
    internal class Helper
    {
        public static bool BackupFile(string path)
        {
            if (File.Exists(path))
            {
                File.Copy(path, path + ".BAK", true);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Asks and gets user input. The user is prompted as long as the
        /// input is empty.
        /// </summary>
        /// <returns>The character entered by the user.</returns>
        public static char GetUserKey()
        {
            string input;

            do
                input = Console.ReadLine();
            while (String.IsNullOrEmpty(input));

            return input[0];
        } // GetUserKey()

        /// <summary>
        /// Allows for invisible user input.
        /// </summary>
        /// <returns>The string typed by the user.</returns>
        public static string HiddenInput()
        {
            StringBuilder sb = new StringBuilder();

            // Read the user input until the <Enter> key has been pressed.
            while (true)
            {
                var key = Console.ReadKey(true);

                if (key.Key == ConsoleKey.Enter)
                    break;
                else if (key.Key == ConsoleKey.Backspace 
                         && sb.Length > 0)
                    sb.Remove(sb.Length - 1, 1);
                else
                {
                    if ((key.Modifiers & ConsoleModifiers.Shift) == 0)
                        sb.Append(key.KeyChar);
                    else
                        sb.Append(key.KeyChar.ToString().ToUpper());
                }
            }

            return sb.ToString();
        } // HiddenInput()

        /// <summary>
        /// Computes the positive modulo of two numbers.
        /// </summary>
        /// <param name="a">The dividend.</param>
        /// <param name="n">The divisor.</param>
        /// <returns>The unsigned remainder of 'a' and 'n'.</returns>
        public static int Mod(int a, int n)
        {
            return ((a % n) + n) % n;
        } // Mod()

        /// <summary>
        /// Prints a header on the top of the screen. A header constists of a
        /// title surrounded by lines (separators).
        /// </summary>
        /// <param name="menu">The menu that relates to the header.</param>
        /// <exception cref="ArgumentException">If the provided menu type does 
        /// not exists.</exception>
        public static void PrintHeader(Menu menu)
        {
            string s;

            switch (menu)
            {
                case Menu.MAIN:
                    s = Resources.Common.MenuMain;
                    break;
                case Menu.LIST:
                    s = Resources.Common.MenuList;
                    break;
                case Menu.ADD:
                    s = Resources.Common.MenuAdd;
                    break;
                case Menu.DELETE:
                    s = Resources.Common.MenuRemove;
                    break;
                case Menu.EDIT:
                    s = Resources.Common.MenuEdit;
                    break;
                case Menu.MASTER:
                    s = Resources.Common.MenuMaster;
                    break;
                default:
                    throw new ArgumentException("Menu type does not exist.", nameof(menu));
            }

            Console.Clear();
            Console.WriteLine(Resources.Common.Separator);
            Console.WriteLine(s);
            Console.WriteLine(Resources.Common.Separator);
            Console.WriteLine();
        } // PrintHeader()
    }
}
