﻿/// ETML
/// Author: Sebastien TILLE
/// Date: March 19th, 2024

namespace PasswordManager
{
    /// <summary>
    /// Executes the program.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Main entry point of the program.
        /// </summary>
        /// <param name="args">Arguments.</param>
        static void Main(string[] args)
        {
            PasswordManager passwordManager = new PasswordManager();
            passwordManager.Start();
        } // Main()
    }
}
