﻿/// ETML
/// Author: Sebastien TILLE
/// Date: March 27th, 2024

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("PasswordManager.Tests")]

namespace PasswordManager
{
    /// <summary>
    /// This class acts as a "translator" (hence its name) between plain and
    /// cipher texts. It is able to encrypt and decrypt using the following
    /// algorithms:
    ///  - Caesar
    ///  - Vigenere
    /// </summary>
    internal class Translator
    {
        /// <summary>
        /// Maximum number of characters that can be represented in the Windows
        /// 1252 code page.
        /// </summary>
        private const int CP_1252_LENGTH = 256;

        /// <summary>
        /// Key used to encrypt and decrypt with Ceasar's cipher.
        /// </summary>
        private readonly int _caesarKey;

        /// <summary>
        /// Key used to encrypt and decrypt with the Vgenere cipher.
        /// </summary>
        public readonly string _vigenereKey;

        /// <summary>
        /// Alphabet used for encryption.
        /// </summary>
        public readonly List<char> alphabet;

        /// <summary>
        /// Initializes a new <see cref="Translator"/> ready to be used with
        /// the Vigenere cipher.
        /// </summary>
        /// <param name="key">Vigenere key.</param>
        public Translator(string key) 
        {
            _vigenereKey = key;
            alphabet = new List<char>();
            alphabet = GetAlphabet(CP_1252_LENGTH);
        } // Translator()

        /// <summary>
        /// Initializes a new <see cref="Translator"/> ready to be used with
        /// the Caesar cipher.
        /// </summary>
        /// <param name="key">Caesar key.</param>
        public Translator(int key)
        {
            _caesarKey = key;
            alphabet = new List<char>();
            alphabet = GetAlphabet(CP_1252_LENGTH);
        } // Translator()

        /// <summary>
        /// Generates an alphabet of printable characters. If a control
        /// character is found in the sequence 0..length, it is skipped.
        /// This means that in general, the resulting alphabet is
        /// smaller than the given length.
        /// </summary>
        /// <param name="length">Maximum length of the alphabet.</param>
        /// <returns>A list containing only printable characters.</returns>
        public List<char> GetAlphabet(int length)
        {
            // Empty alphabet if already filled
            alphabet.Clear();

            // Only keep printable characters
            for (int i = 0; i < length; ++i)
            {
                char c = (char)i;
                if (!char.IsControl(c))
                    alphabet.Add(c);
            }

            return alphabet;
        } // GetAlphabet()

        /// <summary>
        /// Encrypts or decrypts a string using Caesar's cipher and the key 
        /// provided by the associated <see cref="Translator."/>.
        /// </summary>
        /// <param name="input">The string to encrypt or decrypt.</param>
        /// <param name="encrypt">True to encrypt the input, false to 
        /// decrypt.</param>
        /// <returns>The resulting string.</returns>
        public string Caesar(string input, bool encrypt)
        {
            // The key is inverted if decrypting
            int key = encrypt ? _caesarKey : -_caesarKey;
            StringBuilder output = new StringBuilder();

            // Shifting all elements of the input string by modulo-key positions
            for (int i = 0; i < input.Length; ++i)
            {
                output.Append(
                    alphabet[
                        Helper.Mod(alphabet.IndexOf(input[i]) + key, alphabet.Count)
                    ]
                );
            }

            return output.ToString();
        } // Caesar()

        /// <summary>
        /// Encrypts or decrypts a string using Vigenere's cipher and the key
        /// provided by the associated <see cref="Translator"/>.
        /// </summary>
        /// <param name="input">The string to encrypt or decrypt.</param>
        /// <param name="encrypt">Set to true to encrypt or false to 
        /// decrypt.</param>
        /// <returns>The resulting string.</returns>
        public string Vigenere(string input, bool encrypt)
        {
            StringBuilder output = new StringBuilder();

            // Shifting all elements of the input string by the corresponding
            // index in the key
            for (int i = 0; i < input.Length; ++i)
            {
                output.Append(
                    alphabet[
                        Helper.Mod(encrypt ? alphabet.IndexOf(input[i]) + alphabet.IndexOf(_vigenereKey[i % _vigenereKey.Length])
                                           : alphabet.IndexOf(input[i]) - alphabet.IndexOf(_vigenereKey[i % _vigenereKey.Length]), 
                                   alphabet.Count)
                    ] 
                );
            }

            return output.ToString();
        } // Vigenere()
    }
}
