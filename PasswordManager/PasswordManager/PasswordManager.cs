﻿/// ETML
/// Author: Sebastien TILLE
/// Date: March 19th, 2024

using System;
using System.IO;

namespace PasswordManager
{
    /// <summary>
    /// An offline, CLI based password manager.
    /// </summary>
    internal class PasswordManager
    {
        /// <summary>
        /// Default config location.
        /// </summary>
        private const string PATH_CONFIG = ".\\config.txt";

        /// <summary>
        /// Default database location.
        /// </summary>
        private const string PATH_PASS = ".\\pass.txt";

        /// <summary>
        /// Master password entered by the user.
        /// </summary>
        private string master;

        /// <summary>
        /// File manager used to handle read / write operations.
        /// </summary>
        private FileManager fileManager;

        /// <summary>
        /// Encrypter / decrypter.
        /// </summary>
        private Translator translator;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PasswordManager() {}

        /// <summary>
        /// Starts SillySafe.
        /// </summary>
        public void Start()
        {
            // Check if there is a config file. If there is none, create one.
            if (!File.Exists(PATH_CONFIG))
            {
                // Ask for a new master password
                do
                {
                    Console.Clear();
                    Console.Write("Choose a new master password: ");
                    master = Console.ReadLine();

                    Console.Write("Confirm your master password: ");
                }
                while (!master.Equals(Console.ReadLine()));

                // Write new password to config
                translator = new Translator(master);
                using (StreamWriter writer = new StreamWriter(PATH_CONFIG))
                {
                    writer.Write(translator.Vigenere(master, true));
                    writer.Close();
                }
            }
            else
            {
                do
                {
                    Console.Clear();
                    Console.Write("Enter your master password (hidden): ");
                    master = Helper.HiddenInput();
                    translator = new Translator(master);
                }
                while (master.Length < 0 ||
                       translator.Vigenere(master, true) != File.ReadAllText(PATH_CONFIG));
            }

            fileManager = new FileManager(this, PATH_PASS, PATH_CONFIG, translator);
            MainMenu();
        } // Start()

        /// <summary>
        /// Opens the main menu.
        /// </summary>
        public void MainMenu()
        {
            // Empty the console
            Console.Clear();

            // Print application title
            Console.WriteLine(Resources.Common.Separator);
            Console.WriteLine(Resources.Common.ApplicationTitle);
            Console.WriteLine(Resources.Common.Separator);
            Console.WriteLine();

            // Print main menu
            Console.WriteLine(Resources.Common.ApplicationHelpSelection);
            Console.WriteLine(Resources.Common.MenuMain);
            Console.WriteLine();
            Console.WriteLine(Resources.Common.ApplicationHelpQuit);

            // Handle user input
            char key = Helper.GetUserKey();
            switch (key)
            {
                case '1':
                    fileManager.ListEntryDetails();
                    break;
                case '2':
                    fileManager.AddEntry();
                    break;
                case '3':
                    fileManager.RemoveEntry();
                    break;
                case '4':
                    fileManager.EditEntry();
                    break;
                case '5':
                    fileManager.ChangeMasterPassword();
                    break;
                case 'q':
                    Environment.Exit(0);
                    return;
                default:
                    MainMenu();
                    break;
            }

            MainMenu();
        } // MainMenu()
    }
}
